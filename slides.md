---
title: "Brève introduction au système et à l'architecture"
author: Millian Poquet
bibliography: biblio.bib
theme: Estonia
date: 2022-05-31
lang: en
header-includes:
- |
  ```{=latex}
  \usepackage[backend=biber, style=numeric-comp, sorting=none]{biblatex}
  \usepackage{subcaption}
  \setbeamertemplate{footline}[frame number]
  \renewcommand\dots{\ifmmode\ldots\else\makebox[1em][c]{.\hfil.\hfil.}\thinspace\fi}
  \hypersetup{colorlinks,linkcolor=,urlcolor=estonian-blue}
  \graphicspath{{fig/}{img/}}
  \addbibresource{biblio.bib}

  \newcommand{\eg}{\textit{e.g.,}}

  % Fix pandoc shenanigans
  \setbeamertemplate{section page}{}
  \setbeamertemplate{subsection page}{}
  \AtBeginSection{}
  \AtBeginSubsection{}
  ```
---

### Architecture dite de \textsc{von Neumann}

```{=latex}
\begin{center}
  \includegraphics[width=\textwidth]{von-neumann-arch.pdf}
\end{center}
```

### Mémoire et addressage

```{=latex}
\begin{columns}
  \begin{column}{0.3\textwidth}
    \includegraphics[width=\textwidth]{real-ram.jpg}

    \vspace{5mm}
    \includegraphics[width=\textwidth]{bascule.pdf}
  \end{column}
  \begin{column}{0.8\textwidth}
    \includegraphics[width=\textwidth]{proc-mem-alt.png}
  \end{column}
\end{columns}
```

### Programmation sans système d'exploitation

- Accès direct à tous les périphériques

  - Mémoire (vive, stockages...)
  - Processeur
  - Coprocesseurs (\eg\ GPU)
  - Carte réseau, son...

- Très difficile à programmer

  - Démarrage (processeur mode 16 bits $\rightarrow$ 64 bits)
  - Multi tâche
  - Parallélisme (les processeurs ont des cœurs)

### Système d'exploitation

```{=latex}
\begin{columns}
  \begin{column}{0.25\textwidth}
    \includegraphics[width=1.2\textwidth]{os-overview.pdf}
  \end{column}
  \begin{column}{0.75\textwidth}
    \includegraphics[width=\textwidth]{os-scheduling.pdf}
  \end{column}
\end{columns}
```

### Compilation de programmes

```{=latex}
\hspace*{-8mm}\includegraphics[width=1.15\textwidth]{compilation.pdf}
```

### Instanciation de programmes : processus, threads

```{=latex}
\hspace*{-11mm}\includegraphics[width=1.2\textwidth]{program-process-thread.jpg}
```

### Processus en mémoire

```{=latex}
\centering
\includegraphics[height=.9\textheight]{process-layout.png}
```

### Pile d'exécution

```{=latex}
\centering
\includegraphics[width=\textwidth]{call-stack-overview.pdf}
```

### Pile d'exécution (détails)

```{=latex}
\centering
\includegraphics[height=.9\textheight]{call-stack.png}
```

### Pile d'exécution -- Que se passe-t-il ?

```{=latex}
\vspace*{5mm}
\begin{overlayarea}{\textwidth}{\textheight}
  \begin{figure}
    \centering
    \only<1>{\includegraphics[width=\textwidth]{call-stack-overflow1.pdf}}%
    \only<2>{\includegraphics[width=\textwidth]{call-stack-overflow2.pdf}}%
    \only<3>{\includegraphics[width=\textwidth]{call-stack-overflow3.pdf}}%
    \only<4>{\includegraphics[width=\textwidth]{call-stack-overflow4.pdf}}%
  \end{figure}
\end{overlayarea}
```

### Mémoire virtuelle

```{=latex}
\begin{center}
  \includegraphics[height=.9\textheight]{virtual-memory.pdf}
\end{center}
```

### Mémoire virtuelle (pages)

```{=latex}
\begin{center}
  \includegraphics[height=.9\textheight]{virtual-memory2.pdf}
\end{center}
```

### Bases mathématiques (binaire, héxadécimal, décimal)

```{=latex}
\vspace{2mm}
\begin{columns}
  \begin{column}{0.5\textwidth}
    \includegraphics[width=\textwidth]{arithmetic-bases.pdf}
  \end{column}
  \begin{column}{0.38\textwidth}
    \includegraphics[width=\textwidth]{hexa-decimal-binary.png}
  \end{column}
\end{columns}
```

### Variables et mémoire

```{=latex}
\begin{center}
  \includegraphics[width=\textwidth]{variables-in-memory.png}
\end{center}
```

### Processeur et caches

```{=latex}
\begin{center}
  \includegraphics[width=\textwidth]{i7-cache-hierarchy.png}
\end{center}
```

### References

```{=latex}
Source des figures

\tiny
\begin{itemize}
  \item \url{https://sites.uclouvain.be/SystInfo/notes/Theorie/html/MemoireVirtuelle/vmem.html}
  \item \url{https://commons.wikimedia.org/wiki/File:Von_Neumann_Architecture.svg}
  \item \url{https://commons.wikimedia.org/wiki/File:Virtual_memory.svg}
  \item \url{https://commons.wikimedia.org/wiki/File:Operating_system_placement.svg}
  \item \url{https://commons.wikimedia.org/wiki/File:Kernel_Layout.svg}
  \item \url{https://en.wikipedia.org/wiki/File:Concepts-_Program_vs._Process_vs._Thread.jpg}
  \item \url{https://commons.wikimedia.org/wiki/File:Ordonnanceur_noyau_os.svg}
  \item \url{https://commons.wikimedia.org/wiki/File:Flipflop_SR2.svg}
  \item \url{https://javascript.plainenglish.io/node-call-stack-explained-fd9df1c49d2e}
  \item \url{https://commons.wikimedia.org/wiki/File:Virtual_address_space_and_physical_address_space_relationship.svg}
  \item \url{https://man7.org/tlpi/}
  \item \url{https://www3.ntu.edu.sg/home/ehchua/programming/cpp/cp4_PointerReference.html}
  \item \url{https://fr.wikipedia.org/wiki/Système_hexadécimal}
\end{itemize}
```

<!-- ### References {.allowframebreaks}
```{=latex}
\printbibliography[heading=none]
``` -->
